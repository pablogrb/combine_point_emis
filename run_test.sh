#!/bin/bash
# rm class_ptfile.mod
# rm combine_point_emis
# ifort class_ptfile.f90 combine_point_emis.f90 -o combine_point_emis -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source
# ifort class_ptfile.f90 combine_point_emis.f90 -o combine_point_emis -O2 -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

rm point.camx.20130702.bin

./combine_point_emis << EOF
13121
13122
point.camx.20130702.bin
7
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/c3marine/point.c3marine.camx.20130702.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/othpt/point.othpt.camx.20130709.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/pt_oilgas/point.pt_oilgas.camx.20130709.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/ptegu/point.ptegu.camx.20130712.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/ptegu_pk/point.ptegu_pk.camx.20130712.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/ptfire/point.ptfire.camx.20130712.bin
/Users2/pablogar/cmaq2camx_out/12EUS/pnt/ptnonipm/point.ptnonipm.camx.20130709.bin
EOF

# ./combine_point_emis << EOF
# 13121
# 13122
# point.camx.20130507.bin
# 7
# point.c3marine.camx.20130507.bin
# point.othpt.camx.20130507.bin
# point.ptegu.camx.20130501.bin
# point.ptegu_pk.camx.20130501.bin
# point.ptfire.camx.20130501.bin
# point.ptnonipm.camx.20130507.bin
# point.pt_oilgas.camx.20130507.bin
# EOF

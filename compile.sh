#!/bin/bash
ifort class_uam_iv.f90 combine_point_emis.f90 -o combine_point_emis -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source
ifort class_uam_iv.f90 combine_area_emis.f90 -o combine_area_emis -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source


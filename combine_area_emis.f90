!	------------------------------------------------------------------------------------------
!	Main Program
!	------------------------------------------------------------------------------------------

PROGRAM combine_area_emis

USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Combines multiple UAM-IV area source files into a single area source file
!	Inputs:
!		Initial and End date. Assumes 24 hours of hourly data
!		Number of files to be processed
!		Names of files to be processed (2 or more)
!	Outputs:
!		Single area source file with all sources combined
!	By:
!		Shayak Sengupta
!		08-2017
!		Based on combine_point_emis by Pablo Garcia and convert.prep.area.scrappt.f by Laura Posner
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

!	Variables
	INTEGER :: strdate, enddate						! Start and end dates
	INTEGER :: temp_endate							! Temporary date placeholder
	INTEGER :: nfiles								! Number of files to process
	INTEGER :: comb_nspec							! Combined number of species
  INTEGER, ALLOCATABLE :: spec_loc_arr(:,:)		! Species location array (nfiles,comb_nspec)

	CHARACTER(LEN=10), ALLOCATABLE :: long_speclist(:)	! List of all species
	CHARACTER(LEN=10), ALLOCATABLE :: short_speclist(:)	! List of unique species

	CHARACTER(LEN=256) :: out_file					! Output filename

!	Counters
	INTEGER :: i_nf, i_ncs, i_hr, i_nx, i_ny, i_carry, i_ns
	INTEGER :: j

! 	Logicals
	LOGICAL :: in_list

! 	Format strings
	CHARACTER(LEN=17) :: hformat

! 	Data type module
	TYPE(UAM_IV), ALLOCATABLE :: area(:)				! Input area source files
	TYPE(UAM_IV) :: area_out							! Combined area source output

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
	WRITE(*,*) 'Combine multiple UAM-IV area source files into a single area source file'

	WRITE(*,*) 'Start date (YYDDD)'
	READ (*,'(i)') strdate
	WRITE(*,*) 'The start date is ', strdate
	WRITE(*,*) 'End date (YYDDD)'
	READ (*,'(i)') enddate
	WRITE(*,*) 'The end date is ', enddate

	WRITE(*,*) 'Output file name: '
	READ (*,'(a)') area_out%in_file
! 	OPEN (10,FILE=TRIM(out_file),STATUS='NEW')
! 	WRITE(*,*) 'Opened file: ',TRIM(out_file)

	WRITE(*,*) 'Number of files to process'
	READ (*,'(i)') nfiles
	WRITE(*,*) 'Processing ', nfiles, ' files'

!	------------------------------------------------------------------------------------------
!	Combine the data
!	------------------------------------------------------------------------------------------
!
!	Allocate the file structure array 'area'
!	This is an 'nfiles' array of 'UAM_IV' derived type structures
	ALLOCATE(area(nfiles))

! 	Loop to open input files
	DO i_nf = 1,nfiles

! 		Get file name
		WRITE(*,*) 'Input file name: '
		READ (*,'(a)') area(i_nf)%in_file
! 		Assign unit
		area(i_nf)%unit = 20 + i_nf
! 		Read the area source file
		CALL read_uamfile(area(i_nf))

	END DO

!	------------------------------------------------------------------------------------------
! 	Process the species list for combination
	ALLOCATE(long_speclist(SUM(area%nspec)))
	long_speclist = '          ' ! 10 space string
	i_carry = 1
! 	Do the first file
	DO i_ns = 1,area(1)%nspec
		long_speclist(i_carry)=area(1)%c_spname(i_ns)
		i_carry = i_carry + 1
	END DO

! 	Do the other files
	IF (nfiles.GT.1) THEN
!	 	Files loop
		DO i_nf = 2,nfiles
! 			Species loop
			DO i_ns = 1,area(i_nf)%nspec
! 				Compare against species in the long list
				in_list = .FALSE.
				DO i_ncs = 1,i_carry - 1
					IF (area(i_nf)%c_spname(i_ns).EQ.long_speclist(i_ncs)) THEN
						in_list = .TRUE.
						EXIT
					END IF
				END DO
! 				If not in list, then add to the long list
				IF (.NOT.in_list) THEN
					long_speclist(i_carry) = area(i_nf)%c_spname(i_ns)
					i_carry = i_carry + 1
				END IF
			END DO
		END DO
	END IF

! 	Write the short list
	comb_nspec = i_carry - 1
!	area_out%nspec = comb_nspec
	ALLOCATE(short_speclist(comb_nspec))
	DO i_ns = 1,comb_nspec
		short_speclist(i_ns) = long_speclist(i_ns)
	END DO
	DEALLOCATE(long_speclist)
	WRITE(*,*) 'Combined species list'
	WRITE(*,*) short_speclist

! 	Species location array
	ALLOCATE(spec_loc_arr(nfiles,comb_nspec))
	spec_loc_arr = 0
! 	Files loop
	DO i_nf = 1,nfiles
! 		Combined species loop
		DO i_ncs = 1,comb_nspec
! 			Species loop
			DO i_ns = 1,area(i_nf)%nspec
				IF (short_speclist(i_ncs).EQ.area(i_nf)%c_spname(i_ns)) THEN
					spec_loc_arr(i_nf,i_ncs)=i_ns
					EXIT
				END IF
			END DO
		END DO
	END DO
 	WRITE(*,'(2(i3,1x))') spec_loc_arr
!------------------------------------------------------------------------------------------------
!	Build the combined emissions section
!   Clone the headers
    CALL clone_header(area(1),area_out) !use the first file, make sure its a file with unique daily files
    !rewrite the number of species
    area_out%nspec = comb_nspec

! 	Allocate the header arrays
	ALLOCATE(area_out%ibgdat(area_out%update_times), area_out%iendat(area_out%update_times))
	ALLOCATE(area_out%nbgtim(area_out%update_times), area_out%nentim(area_out%update_times))

! 	Allocate the emissions array
	ALLOCATE(area_out%aemis(area_out%nx,area_out%ny,area_out%update_times,area_out%nspec))
! 	Initiate the emissions array in zeros
	area_out%aemis = 0.

! 	Loop over hours
	DO i_hr = 1,area_out%update_times	! Update times is default 24

! 		This assumes 24 hour long update times, change as necessary
! 		Fits the SOAS dataset
! 		Build the section header
		IF (i_hr.NE.24) THEN
			temp_endate = strdate
		ELSE
			temp_endate = enddate
		END IF
		area_out%ibgdat(i_hr) = strdate
		area_out%nbgtim(i_hr) = area(1)%nbgtim(i_hr)	! Get hour from the first file
		area_out%iendat(i_hr) = temp_endate
		area_out%nentim(i_hr) = area(1)%nentim(i_hr)	! Get hour from the first file

! 		Output the section header
		hformat = '(5x,2(i10,f10.2))'
		WRITE(*,hformat) area_out%ibgdat(i_hr), area_out%nbgtim(i_hr), area_out%iendat(i_hr)&
			&,area_out%nentim(i_hr)

! ! 		Files loop
! 		DO i_nf = 1,nfiles
! ! 			Output the file name
! 			WRITE(*,*) 'Working on: ',TRIM(area(i_nf)%in_file)
! ! 			X loop
! 			DO i_nx = 1,area(i_nf)%nx
!                         WRITE(*,*) 'X LOOP'
! !				Y loop
! 			 	DO i_ny = 1,area(i_nf)%ny
!                                 WRITE(*,*) 'Y LOOP'
! ! 				Loop through species
! 					DO i_ncs = 1,comb_nspec
!                                         WRITE(*,*) 'SPECIES LOOP'
! ! 					Get the location of the species in the file array
! 						i_ns = spec_loc_arr(i_nf,i_ncs)
!                                                 WRITE(*,*)'i_ns = ',i_ns
! 						IF (i_ns.NE.0) THEN
! !	 					    Get the emission
! 							area_out%aemis(i_nx,i_ny,i_hr,i_ncs) = area_out%aemis(i_nx,i_ny,i_hr,i_ncs) &
! 							& + area(i_nf)%aemis(i_nx,i_ny,i_hr,i_ns)
! 						END IF
! 					END DO
! 				END DO
! 			END DO
! 		END DO
		! Species loop
		DO i_ncs = 1,comb_nspec
			! Files loop
			DO i_nf = 1,nfiles
				! Get the location of the species in the file array
				i_ns = spec_loc_arr(i_nf,i_ncs)
				IF (i_ns .NE. 0) THEN
					area_out%aemis(:,:,i_hr,i_ncs) = area_out%aemis(:,:,i_hr,i_ncs) + area(i_nf)%aemis(:,:,i_hr,i_ns)
				END IF
			END DO
		END DO
	END DO

!	------------------------------------------------------------------------------------------
!	Write the output file
!	------------------------------------------------------------------------------------------
!

! 	Build the species list using the combined list
	ALLOCATE(area_out%spname(10,comb_nspec))
	ALLOCATE(area_out%c_spname(comb_nspec))
	DO i_ncs = 1,comb_nspec
		DO j = 1,10
			area_out%spname(j,i_ncs) = short_speclist(i_ncs)(j:j)
		END DO
	END DO
	area_out%c_spname = short_speclist

! 	Write the output file
	CALL write_uamfile(area_out)

END PROGRAM combine_area_emis


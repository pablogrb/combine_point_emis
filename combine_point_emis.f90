!	------------------------------------------------------------------------------------------
!	Main Program
!	------------------------------------------------------------------------------------------

PROGRAM combine_point_emis

USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Combines multiple UAM-IV point source files into a single point source file
!	Inputs:
!		Initial and End date. Assumes 24 hours of hourly data
!		Number of files to be processed
!		Names of files to be processed (2 or more)
!	Outputs:
!		Single point source file with all sources combined
!	By:
!		Pablo Garcia
!		04-2016
!		Based on s_after_SAPRC and bin2asc
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

!	Variables
	INTEGER :: strdate, enddate						! Start and end dates
	INTEGER :: temp_endate							! Temporary date placeholder
	INTEGER :: nfiles								! Number of files to process
	INTEGER :: comb_nspec							! Combined number of species
	INTEGER, ALLOCATABLE :: spec_loc_arr(:,:)		! Species location array (nfiles,comb_nspec)

	CHARACTER(LEN=10), ALLOCATABLE :: long_speclist(:)	! List of all species
	CHARACTER(LEN=10), ALLOCATABLE :: short_speclist(:)	! List of unique species

	CHARACTER(LEN=256) :: out_file					! Output filename

!	Counters
	INTEGER :: i_nf, i_ns, i_carry, i_ncs, i_stk, i_hr
	INTEGER :: j

! 	Logicals
	LOGICAL :: in_list

! 	Format strings
	CHARACTER(LEN=17) :: hformat

! 	Data type module
	TYPE(UAM_IV), ALLOCATABLE :: pt(:)				! Input point source files
	TYPE(UAM_IV) :: pt_out							! Combined point source output

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
	WRITE(*,*) 'Combine multiple UAM-IV point source files into a single point source file'

	WRITE(*,*) 'Start date (YYDDD)'
	READ (*,'(i)') strdate
	WRITE(*,*) 'The start date is ', strdate
	WRITE(*,*) 'End date (YYDDD)'
	READ (*,'(i)') enddate
	WRITE(*,*) 'The end date is ', enddate

	WRITE(*,*) 'Output file name: '
	READ (*,'(a)') pt_out%in_file
! 	OPEN (10,FILE=TRIM(out_file),STATUS='NEW')
! 	WRITE(*,*) 'Opened file: ',TRIM(out_file)

	WRITE(*,*) 'Number of files to process'
	READ (*,'(i)') nfiles
	WRITE(*,*) 'Processing ', nfiles, ' files'

!	------------------------------------------------------------------------------------------
!	Combine the data
!	------------------------------------------------------------------------------------------
!
!	Allocate the file structure array 'pt'
!	This is an 'nfiles' array of 'UAM_IV' derived type structures
	ALLOCATE(pt(nfiles))

! 	Loop to open input files
	DO i_nf = 1,nfiles

! 		Get file name
		WRITE(*,*) 'Input file name: '
		READ (*,'(a)') pt(i_nf)%in_file
! 		Assign unit
		pt(i_nf)%unit = 20 + i_nf
! 		Read the point source file
		CALL read_uamfile(pt(i_nf))

	END DO

!	------------------------------------------------------------------------------------------
! 	Process the species list for combination
	ALLOCATE(long_speclist(SUM(pt%nspec)))
	long_speclist = '          ' ! 10 space string
	i_carry = 1
! 	Do the first file
	DO i_ns = 1,pt(1)%nspec
		long_speclist(i_carry)=pt(1)%c_spname(i_ns)
		i_carry = i_carry + 1
	END DO

! 	Do the other files
	IF (nfiles.GT.1) THEN
!	 	Files loop
		DO i_nf = 2,nfiles
! 			Species loop
			DO i_ns = 1,pt(i_nf)%nspec
! 				Compare against species in the long list
				in_list = .FALSE.
				DO i_ncs = 1,i_carry - 1
					IF (pt(i_nf)%c_spname(i_ns).EQ.long_speclist(i_ncs)) THEN
						in_list = .TRUE.
						EXIT
					END IF
				END DO
! 				If not in list, then add to the long list
				IF (.NOT.in_list) THEN
					long_speclist(i_carry) = pt(i_nf)%c_spname(i_ns)
					i_carry = i_carry + 1
				END IF
			END DO
		END DO
	END IF

! 	Write the short list
	comb_nspec = i_carry - 1
	pt_out%nspec = comb_nspec
	ALLOCATE(short_speclist(comb_nspec))
	DO i_ns = 1,comb_nspec
		short_speclist(i_ns) = long_speclist(i_ns)
	END DO
	DEALLOCATE(long_speclist)
	WRITE(*,*) 'Combined species list'
	WRITE(*,*) short_speclist

! 	Species location array
	ALLOCATE(spec_loc_arr(nfiles,comb_nspec))
	spec_loc_arr = 0
! 	Files loop
	DO i_nf = 1,nfiles
! 		Combined species loop
		DO i_ncs = 1,comb_nspec
! 			Species loop
			DO i_ns = 1,pt(i_nf)%nspec
				IF (short_speclist(i_ncs).EQ.pt(i_nf)%c_spname(i_ns)) THEN
					spec_loc_arr(i_nf,i_ncs)=i_ns
					EXIT
				END IF
			END DO
		END DO
	END DO
! 	WRITE(*,'(7(i2,1x))') spec_loc_arr

!	------------------------------------------------------------------------------------------
!	Calculate the total number of point sources
	pt_out%nstk = SUM(pt%nstk)

! 	Allocate the stack parameter arrays
	ALLOCATE(pt_out%xstk(pt_out%nstk), pt_out%ystk(pt_out%nstk))
	ALLOCATE(pt_out%hstk(pt_out%nstk), pt_out%dstk(pt_out%nstk))
	ALLOCATE(pt_out%tstk(pt_out%nstk), pt_out%vstk(pt_out%nstk))

! 	Build the combined stack parameter records
	i_carry = 1
! 	Files loop
	DO i_nf = 1,nfiles
! 		Stack loop
		DO i_stk = 1,pt(i_nf)%nstk
			pt_out%xstk(i_carry) = pt(i_nf)%xstk(i_stk)
			pt_out%ystk(i_carry) = pt(i_nf)%ystk(i_stk)
			pt_out%hstk(i_carry) = pt(i_nf)%hstk(i_stk)
			pt_out%dstk(i_carry) = pt(i_nf)%dstk(i_stk)
			pt_out%tstk(i_carry) = pt(i_nf)%tstk(i_stk)
			pt_out%vstk(i_carry) = pt(i_nf)%vstk(i_stk)
! 			Advance the counter
			i_carry = i_carry + 1
		END DO
	END DO

!	------------------------------------------------------------------------------------------
!	Build the combined emissions section

! 	Allocate the header arrays
	ALLOCATE(pt_out%ibgdat(pt_out%update_times), pt_out%iendat(pt_out%update_times))
	ALLOCATE(pt_out%nbgtim(pt_out%update_times), pt_out%nentim(pt_out%update_times))

! 	Allocate the stack description arrays
	ALLOCATE(pt_out%icell(pt_out%update_times,pt_out%nstk))
	ALLOCATE(pt_out%jcell(pt_out%update_times,pt_out%nstk))
	ALLOCATE(pt_out%kcell(pt_out%update_times,pt_out%nstk))
	ALLOCATE(pt_out%flow(pt_out%update_times,pt_out%nstk))
	ALLOCATE(pt_out%plmht(pt_out%update_times,pt_out%nstk))

! 	Allocate the emissions array
	ALLOCATE(pt_out%ptemis(pt_out%update_times,pt_out%nstk,pt_out%nspec))
! 	Initiate the emissions array in zeros
	pt_out%ptemis = 0.

! 	Loop over hours
	DO i_hr = 1,pt_out%update_times	! Update times is default 24
! 		This assumes 24 hour long update times, change as necessary
! 		Fits the SOAS dataset
! 		Build the section header
		IF (i_hr.NE.24) THEN
			temp_endate = strdate
		ELSE
			temp_endate = enddate
		END IF
		pt_out%ibgdat(i_hr) = strdate
		pt_out%nbgtim(i_hr) = pt(1)%nbgtim(i_hr)	! Get hour from the first file
		pt_out%iendat(i_hr) = temp_endate
		pt_out%nentim(i_hr) = pt(1)%nentim(i_hr)	! Get hour from the first file

! 		Output the section header
		hformat = '(5x,2(i10,f10.2))'
		WRITE(*,hformat) pt_out%ibgdat(i_hr), pt_out%nbgtim(i_hr), pt_out%iendat(i_hr)&
			&,pt_out%nentim(i_hr)

! 		Build the stack description arrays
		i_carry = 1
! 		Files loop
		DO i_nf = 1,nfiles
! 			Output the file name
			WRITE(*,*) 'Working on: ',TRIM(pt(i_nf)%in_file)
! 			Stack loop
			DO i_stk = 1,pt(i_nf)%nstk
! 				Build the point source descriptions
				pt_out%icell(i_hr,i_carry) = pt(i_nf)%icell(i_hr,i_stk)
				pt_out%jcell(i_hr,i_carry) = pt(i_nf)%jcell(i_hr,i_stk)
				pt_out%kcell(i_hr,i_carry) = pt(i_nf)%kcell(i_hr,i_stk)
				pt_out%flow(i_hr,i_carry)  = pt(i_nf)%flow(i_hr,i_stk)
				pt_out%plmht(i_hr,i_carry) = pt(i_nf)%plmht(i_hr,i_stk)
! 				Loop through species
				DO i_ncs = 1,comb_nspec
! 					Get the location of the species in the file array
					i_ns = spec_loc_arr(i_nf,i_ncs)
					IF (i_ns.NE.0) THEN
!	 					Get the emision
						pt_out%ptemis(i_hr,i_carry,i_ncs) = pt(i_nf)%ptemis(i_hr,i_stk,i_ns)
					END IF
				END DO
! 				Advance the counter
				i_carry = i_carry + 1
			END DO
		END DO
	END DO


!	------------------------------------------------------------------------------------------
!	Write the output file
!	------------------------------------------------------------------------------------------
!

	CALL clone_header(pt(1), pt_out)

!	Build the output file header using the header of the first file and the user dates
! 	Header 1
!	pt_out%fname  = pt(1)%fname
!	pt_out%note   = pt(1)%note
!	pt_out%nseg   = pt(1)%nseg
!	pt_out%nspec  = pt(1)%nspec
	pt_out%idate  = strdate		! User start date
!	pt_out%begtim = pt(1)%begtim
	pt_out%jdate  = enddate		! User end date
!	pt_out%endtim = pt(1)%endtim
! 	Header 2
!	pt_out%orgx = pt(1)%orgx
!	pt_out%orgy = pt(1)%orgy
!	pt_out%iutm = pt(1)%iutm
!	pt_out%utmx = pt(1)%utmx
!	pt_out%utmy = pt(1)%utmy
!	pt_out%dx   = pt(1)%dx
!	pt_out%dy   = pt(1)%dy
!	pt_out%nx   = pt(1)%nx
!	pt_out%ny   = pt(1)%ny
!	pt_out%nz   = pt(1)%nz
!	pt_out%nzlo = pt(1)%nzlo
!	pt_out%nzup = pt(1)%nzup
!	pt_out%hts  = pt(1)%hts
!	pt_out%htl  = pt(1)%htl
!	pt_out%htu  = pt(1)%htu
! 	Header 3
!	pt_out%i1  = pt(1)%i1
!	pt_out%j1  = pt(1)%j1
!	pt_out%nx1 = pt(1)%nx1
!	pt_out%ny1 = pt(1)%ny1

! 	Build the species list using the combined list
	ALLOCATE(pt_out%spname(10,comb_nspec))
	ALLOCATE(pt_out%c_spname(comb_nspec))
	DO i_ncs = 1,comb_nspec
		DO j = 1,10
			pt_out%spname(j,i_ncs) = short_speclist(i_ncs)(j:j)
		END DO
	END DO
	pt_out%c_spname = short_speclist

! 	Write the output file
	CALL write_uamfile(pt_out)

END PROGRAM combine_point_emis
